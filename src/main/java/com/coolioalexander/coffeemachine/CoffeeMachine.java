package com.coolioalexander.coffeemachine;

import java.math.BigDecimal;
import java.util.Objects;

public class CoffeeMachine {

    private final DrinkMaker drinkMaker;
    private final CashRegister cashRegister;

    public CoffeeMachine(DrinkMaker drinkMaker, CashRegister cashRegister) {
        this.drinkMaker = drinkMaker;
        this.cashRegister = cashRegister;
    }

    public DrinkMaker getDrinkMaker() {
        return drinkMaker;
    }

    public void take(Order order, BigDecimal amountInserted) {
        Objects.requireNonNull(amountInserted, "Inserted money must not be null");

        if (amountInserted.compareTo(order.getDrink().getPrice()) < 0) {
            BigDecimal missingAmount = order.getDrink().getPrice().subtract(amountInserted);
            drinkMaker.missMoney("M:" + missingAmount);
        } else {
            boolean success = drinkMaker.process(order.instructions());
            if (success)
                cashRegister.register(order);
        }
    }

    public void printReport() {
        System.out.println(cashRegister.report());
    }

}
