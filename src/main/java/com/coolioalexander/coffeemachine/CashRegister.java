package com.coolioalexander.coffeemachine;

import java.math.BigDecimal;
import java.util.*;

public class CashRegister {

    private final Map<Drink, Integer> sales = new HashMap<>();
    private BigDecimal sold;

    public CashRegister() {
        resetSales();
        this.sold = BigDecimal.ZERO;
    }

    private void resetSales() {
        List<Drink> hotDrinks = Arrays.asList(HotDrink.values());
        List<Drink> coldDrinks = Arrays.asList(ColdDrink.values());
        List<Drink> drinks = new ArrayList<>();
        drinks.addAll(hotDrinks);
        drinks.addAll(coldDrinks);

        drinks.forEach(drink -> sales.put(drink, 0));
    }

    public void register(Order order) {
        int count = sales.get(order.getDrink());
        sales.put(order.getDrink(), ++count);
        sold = sold.add(order.getDrink().getPrice());
    }

    public String report() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Drink, Integer> sale : sales.entrySet()) {
            builder.append(sale.getKey().toString()).append(": ")
                    .append(sale.getValue())
                    .append("\n");
        }
        builder.append("Total: ")
                .append(sold);
        return builder.toString();
    }

    public Map<Drink, Integer> getSales() {
        return sales;
    }

    public BigDecimal getSold() {
        return sold;
    }

}
