package com.coolioalexander.coffeemachine;

import java.math.BigDecimal;

public enum HotDrink implements Drink {
    TEA("T", new BigDecimal("0.4")),
    COFFEE("C", new BigDecimal("0.6")),
    CHOCOLATE("H", new BigDecimal("0.5"));

    private final String code;
    private final BigDecimal price;

    HotDrink(String code, BigDecimal price) {
        this.code = code;
        this.price = price;
    }

    @Override
    public String getCode() {
        return code + "h";
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

}
