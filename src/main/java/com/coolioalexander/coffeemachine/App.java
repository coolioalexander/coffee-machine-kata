package com.coolioalexander.coffeemachine;

import java.math.BigDecimal;

public class App {

    private static CoffeeMachine coffeeMachine;

    public static void main(String[] args) {
        DrinkMaker drinkMaker = new DrinkMaker() {
            @Override
            public boolean process(String instructions) {
                return true;
            }

            @Override
            public void missMoney(String message) {
            }
        };
        CashRegister cashRegister = new CashRegister();
        coffeeMachine = new CoffeeMachine(drinkMaker, cashRegister);

        System.out.println();
        Order order = new Order(HotDrink.TEA, 1);
        orderDrink(order, new BigDecimal("0.4"));
        order = new Order(HotDrink.CHOCOLATE);
        orderDrink(order, new BigDecimal("0.1"));
        order = new Order(HotDrink.COFFEE, 2);
        orderDrink(order, new BigDecimal("0.6"));
        order = new Order(ColdDrink.ORANGE_JUICE);
        orderDrink(order, new BigDecimal("0.6"));
        order = new Order(HotDrink.CHOCOLATE, 1);
        orderDrink(order, new BigDecimal("1"));
        order = new Order(HotDrink.COFFEE);
        orderDrink(order, null);
        order = new Order(ColdDrink.ORANGE_JUICE);
        orderDrink(order, new BigDecimal("0.7"));

        System.out.println();
        coffeeMachine.printReport();
    }

    private static void orderDrink(Order order, BigDecimal insertedAmount) {
        try {
            coffeeMachine.take(order, insertedAmount);
            System.out.println(
                    "Order of "
                    + order.getDrink().toString()
                            + " - "
                    + order.instructions()
                            + " sent to Drink Maker");
        } catch (NullPointerException | IllegalArgumentException ex) {
            System.out.println("order of " + order.getDrink().toString() + " fails");
        }
    }

}
