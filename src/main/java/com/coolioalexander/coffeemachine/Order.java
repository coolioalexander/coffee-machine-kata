package com.coolioalexander.coffeemachine;

import java.util.Objects;

public class Order {

    private final Drink drink;
    private final int sugarQuantity;

    public Order(Drink drink) {
        this(drink, 0);
    }

    public Order(Drink drink, int sugarQuantity) {
        this.drink = Objects.requireNonNull(drink, "Drink must not be null");
        this.sugarQuantity = requireQuantity(sugarQuantity);
    }

    private int requireQuantity(int quantity) {
        if (quantity < 0 || quantity > 2)
            throw new IllegalArgumentException("Quantity must be between 0 and 2");
        return quantity;
    }

    public String instructions() {
        return String.format("%s:%s:%s", drink.getCode(), quantity(), stirrer());
    }

    private String quantity() {
        return sugarQuantity == 0 ? "" : String.valueOf(sugarQuantity);
    }

    private String stirrer() {
        return sugarQuantity == 0 ? "" : "0";
    }

    public Drink getDrink() {
        return drink;
    }

}
