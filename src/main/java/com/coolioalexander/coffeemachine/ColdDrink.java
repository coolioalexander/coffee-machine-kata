package com.coolioalexander.coffeemachine;

import java.math.BigDecimal;

public enum ColdDrink implements Drink {
    ORANGE_JUICE("O", new BigDecimal("0.6"));

    private final String code;
    private final BigDecimal price;

    ColdDrink(String code, BigDecimal price) {
        this.code = code;
        this.price = price;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

}
