package com.coolioalexander.coffeemachine;

public interface DrinkMaker {
    boolean process(String instructions);

    void missMoney(String message);

}
