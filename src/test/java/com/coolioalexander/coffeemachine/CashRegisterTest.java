package com.coolioalexander.coffeemachine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CashRegisterTest {

    private CashRegister cashRegister;

    @BeforeEach
    void init() {
        cashRegister = new CashRegister();

        Order order = new Order(HotDrink.TEA, 1);
        cashRegister.register(order);
        order = new Order(HotDrink.CHOCOLATE);
        cashRegister.register(order);
        order = new Order(HotDrink.COFFEE, 2);
        cashRegister.register(order);
        order = new Order(ColdDrink.ORANGE_JUICE);
        cashRegister.register(order);
        order = new Order(HotDrink.CHOCOLATE, 1);
        cashRegister.register(order);
    }

    @Test
    void shouldRegisterSales() {
        assertThat(cashRegister)
                .extracting(
                        it -> it.getSales().get(HotDrink.TEA),
                        it -> it.getSales().get(HotDrink.CHOCOLATE),
                        it -> it.getSales().get(HotDrink.COFFEE),
                        it -> it.getSales().get(ColdDrink.ORANGE_JUICE),
                        CashRegister::getSold
                )
                .containsExactly(1, 2, 1, 1, new BigDecimal("2.6"));
    }

    @Test
    void shouldReportSales() {
        String report = cashRegister.report();

        assertThat(report)
                .contains("TEA: 1")
                .contains("CHOCOLATE: 2")
                .contains("COFFEE: 1")
                .contains("ORANGE_JUICE: 1")
                .contains("Total: 2.6");
    }

}
